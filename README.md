# About Ear Talk
Ear Talk project enables people from remote locations to collaboratively share, shape, and form music through an interactive score. The Ear Talk system incorporates YouTube Live platform as a playground for people to showcase their sounds and interact with them through the YouTube comment. For example, you can comment “Hey Ear Talk, please make sample1 louder by 60%!” Through this type of interaction, we challenge viewers to amalgamate sounds of different origins into one coherent piece.  

The idea was inspired by our desire to share photos and videos on social media. However, by limiting the shareable content to merely sound, this project challenges participants to pay closer attention to the sonic qualities of our environment. The Max/Jitter hosted program “misuses” the Google ecosystem in order to collect audio files as they are shared on Google Drive, organize them into a visual score that is then live streamed on YouTube, and facilitate participants’ interactions with the score through the YouTube comment section via Google API.  

More information can be found on collaborator Toshihisa Tsuruoka's website [here](https://www.toshihisatsuruoka.com/eartalk) and ensemble Consensus's website [here](https://www.ensembleconsensus.org/ear-talk).  
Research paper to be presented on the project at the International Computer Music Confrence can be found [here](https://drive.google.com/file/d/1DibSdYSkMlINj4Gif8jMQaUJS5_HlHfg/view).
# About the backend
With the release of [Max 8](https://cycling74.com/products/max), Node.js has been implemented to run directly in the Max patching enviroment.
This addition allows for artists to run full scale Node applications. Ear Talk leverages this feature to call the YouTube Data API to retrieve the chat messages from a Live stream. It stores them in a SQLite database and returns any new messages since the last poll to Max.  
## Running the backend
![alt text](max_example.png "Max Example")
1. Get a YouTube Data API key
2. Clone this repository and add you API key to the creds.json where appropriate
3. Make sure you have Max 8 installed. Open *dataToMaxExample.maxpat*
4. Hit the *script npm install* message
5. Go to YouTube live stream and copy the url, unlock the patch and update the message with that
6. The number is how often to poll in milliseconds, change as desired
7. Lock the patch and hit the script start message.
8. Hit the toggle connected to the *query $1* message. If everything is setup, you should start seeming new messages soon.